import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		createAuthor(){
			let  first_name = this.get('first_name')
			let last_name = this.get('last_name')
			let  nickname = this.get('nickname')
			let age = this.get('age')

			this.store.createRecord('author', {
				first_name: first_name,
				last_name: last_name,
				nickname: nickname,
				age: age
			}).save()
		}
	}
});
